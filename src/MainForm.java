import javax.swing.*;
import java.awt.event.*;
import java.beans.PropertyChangeListener;

public class MainForm {
    private JPanel mainPanel;
    private JButton collapseButton;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextArea textArea;
    private JButton expandButton;

    public MainForm() {
        textArea.setVisible(false);
        expandButton.setVisible(false);
        collapseButton.addActionListener(new Action() {
            @Override
            public Object getValue(String key) {
                return null;
            }

            @Override
            public void putValue(String key, Object value) {

            }

            @Override
            public void setEnabled(boolean b) {

            }

            @Override
            public boolean isEnabled() {
                return false;
            }

            @Override
            public void addPropertyChangeListener(PropertyChangeListener listener) {

            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener listener) {

            }

            @Override
            public void actionPerformed(ActionEvent e) {
                if (isValidName(textField1.getText(), textField2.getText()) &&
                        !(textField1.getText().isEmpty() && textField2.getText().isEmpty())) {
                    collapseButton.setVisible(false);
                    textField1.setVisible(false);
                    textField2.setVisible(false);
                    textField3.setVisible(false);
                    expandButton.setVisible(true);
                    textArea.setVisible(true);
                    textArea.setText(textField1.getText() + " " + textField2.getText() + " " + textField3.getText());

                } else {
                    JOptionPane.showMessageDialog(mainPanel, "Поля фамилия и имя должны быть заполнены!",
                            "Ошибка", JOptionPane.PLAIN_MESSAGE);
                }
            }
        });

        expandButton.addActionListener(new Action() {
            @Override
            public Object getValue(String key) {
                return null;
            }

            @Override
            public void putValue(String key, Object value) {

            }

            @Override
            public void setEnabled(boolean b) {

            }

            @Override
            public boolean isEnabled() {
                return false;
            }

            @Override
            public void addPropertyChangeListener(PropertyChangeListener listener) {

            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener listener) {

            }

            @Override
            public void actionPerformed(ActionEvent e) {
                if (isValidName(textArea.getText())) {
                    String[] name = textArea.getText().split(" ");
                    switch (name.length) {
                        case 2 -> {
                            textField1.setText(name[0]);
                            textField2.setText(name[1]);
                            textField3.setText("");
                        }
                        case 3 -> {
                            textField1.setText(name[0]);
                            textField2.setText(name[1]);
                            textField3.setText(name[2]);
                        }
                        default -> JOptionPane.showMessageDialog(mainPanel, "Поля фамилия и имя должны быть заполнены!",
                                "Ошибка", JOptionPane.PLAIN_MESSAGE);
                    }
                    expandButton.setVisible(false);
                    collapseButton.setVisible(true);
                    textArea.setVisible(false);
                    textField1.setVisible(true);
                    textField2.setVisible(true);
                    textField3.setVisible(true);
                }
            }
        });
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public static boolean isValidName(String... name) {
        boolean isValid = false;
        for (String s : name) {
            isValid = s.matches("^[a-zA-Zа-яА-Я\\s]+");
        }
        return isValid;
    }

    public static boolean isValidName(String name) {
        return name.matches("^[a-zA-Zа-яА-Я\\s]+");
    }
}
